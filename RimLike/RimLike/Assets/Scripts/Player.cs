using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour{

    [SerializeField] GameObject gameManagerObj;
    [SerializeField] GameObject player;
    public void GetSelected() {
        gameManagerObj.GetComponent<GameManager>().selected = gameObject;
    }

    public void Move(Vector2 pos) {
        //Debug.Log("Told to move to " + pos.x + ", " + pos.y);
        Vector3 goToPos = (Vector3)pos + (new Vector3(0f, 0f, -0.5f));
        
        player.transform.position = goToPos;
    }
}
