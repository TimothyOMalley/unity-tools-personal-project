using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
using UnityEditor;

public class Tile : MonoBehaviour{
    [SerializeField] TileDefinition tileInfo;
    Sprite tileSprite;
    TileDefinition.Type tileType;

    private void OnValidate() {
        tileSprite = tileInfo.Visuals();
        tileType = tileInfo.GetTileType();

        gameObject.GetComponent<SpriteRenderer>().sprite = tileSprite;
    }

}