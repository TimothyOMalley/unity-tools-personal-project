using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node{

    Grid<Node> grid;
    public int x, y;

    public int gCost, hCost, fCost;
    public Node cameFrom;

    public Node(Grid<Node> grid, int x, int y) {
        this.grid = grid;
        this.x = x;
        this.y = y;
    }

    public override string ToString() {
        return x + "," + y;
    }

    public void CalculateFCost() {
        fCost = gCost + hCost;
    }
}
