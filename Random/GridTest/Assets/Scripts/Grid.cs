using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Grid<T>{
    int width, height;
    T[,] varArray;

    public Grid(){
        this.width = width;
        this.height = height;
    }
}


/*
public class Grid<TGridObject>{

    public event EventHandler<OnGridObjectChangedEventArgs> onGridObjectChanged;
    public class OnGridObjectChangedEventArgs: EventArgs {
        public int x, y;
    }

    int width, height;
    float cellSize;
    Vector3 originPos;
    TGridObject[,] gridArray;
    TextMesh[,] debugTextArray;

    [SerializeField] bool debug = true;

    public Grid(int width, int height, float cellSize, Vector3 originPosition) {
        this.width = width;
        this.height = height;
        this.cellSize = cellSize;
        this.originPos = originPosition;
        gridArray = new TGridObject[width, height];
        if(debug) {
            debugTextArray = new TextMesh[width, height];
        }
    }

    public int Width {
        get { return width; }
        //set { width = value; }
    }

    public int Height {
        get { return height; }
        //set { height = value; }
    }

    public void SetValue(int x, int y, TGridObject value) {
        if((x >= 0 && y >= 0) && (x <= width && y <= height)) {
            gridArray[x, y] = value;
            if(onGridObjectChanged != null) onGridObjectChanged(this, new OnGridObjectChangedEventArgs { x = x, y = y });
            if(debug) {
                debugTextArray[x, y].text = value.ToString();
            }
        }
    }

    private void GetXY(Vector3 worldPosition, out int x, out int y) {
        x = Mathf.FloorToInt((worldPosition-originPos).x / cellSize);
        y = Mathf.FloorToInt((worldPosition - originPos).y / cellSize);
    }

    public void SetValue(Vector3 worldPosition, TGridObject value) {
        int x, y;
        GetXY(worldPosition,out x,out y);
        SetValue(x, y, value);

    }

    public TGridObject GetValue(int x, int y) {
        return gridArray[x, y];
    }

    public TGridObject GetValue(Vector3 worldPosition) {
        int x, y;
        GetXY(worldPosition, out x, out y);
        return GetValue(x, y);
    }

    private Vector3 GetWorldPosition(int x,int y) {
        return new Vector3(x, y) * cellSize + originPos;
    }

    public void DrawGrid() {
        for(int x = 0; x < gridArray.GetLength(0); x++) {
            for(int y = 0; y < gridArray.GetLength(1); y++) {
                Debug.DrawLine(GetWorldPosition(x, y), GetWorldPosition(x, y + 1), Color.white, 100f);
                Debug.DrawLine(GetWorldPosition(x, y), GetWorldPosition(x + 1, y), Color.white, 100f);
            }
        }
        Debug.DrawLine(GetWorldPosition(0, height), GetWorldPosition(width, height), Color.white, 100f);
        Debug.DrawLine(GetWorldPosition(width, 0), GetWorldPosition(width, height), Color.white, 100f);
    }

    public void DrawTextOnGrid() {
        for(int x = 0; x < gridArray.GetLength(0); x++) {
            for(int y = 0; y < gridArray.GetLength(1); y++) {
                DrawText(null, GetValue(x,y).ToString(), GetWorldPosition(x,y)+new Vector3(cellSize,cellSize)*0.5f,12,Color.white,TextAnchor.MiddleCenter);
            }
        }
    }

    private void DrawText(Transform parent, string text, Vector3 worldPos, int fontSize, Color textColor, TextAnchor textAnchor) {
        GameObject gameObject = new GameObject("World_Text", typeof(TextMesh));
        int x, y;
        GetXY(worldPos, out x, out y);
        Transform transform = gameObject.transform;
        transform.SetParent(parent, false);
        transform.localPosition = worldPos;
        TextMesh textMesh = gameObject.GetComponent<TextMesh>();
        textMesh.text = text;
        textMesh.fontSize = fontSize;
        textMesh.transform.position = worldPos;
        textMesh.color = textColor;
        textMesh.anchor = textAnchor;
        if(debug) {
            debugTextArray[x, y] = textMesh;
        }
    }

}
*/