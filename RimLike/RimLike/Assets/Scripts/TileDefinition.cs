using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;

[CreateAssetMenu(fileName = "DefaultTileDefinition",menuName ="ScriptableObjects/Tile",order =1)]

public class TileDefinition : ScriptableObject{
    [SerializeField] Sprite visuals;
    public enum Type { undefined, ground }
    [SerializeField] Type type = Type.undefined;

    public Sprite Visuals(){
        return visuals;
    }

    public Type GetTileType() {
        return type;
    }
}
