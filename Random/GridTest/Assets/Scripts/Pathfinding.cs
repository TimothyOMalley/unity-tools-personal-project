using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinding{

    private const int MOVE_STRAIGHT_COST = 10;
    private const int MOVE_DIAGONAL_COST = 14;

    Grid<Node> grid;

    List<Node> openList;
    List<Node> closedList;

    public Pathfinding(int width, int height) {
        grid = new Grid<Node>(width, height, 10f, new Vector3());
    }

    List<Node> FindPath(int startX,int startY,int endX,int endY) {
        Node startNode = grid.GetValue(startX, startY);
        Node endNode = grid.GetValue(endX, endY);

        openList = new List<Node> { startNode };
        closedList = new List<Node>();

        for(int x = 0; x < grid.Width; x++) {
            for(int y = 0; y < grid.Height; y++) {
                Node node = grid.GetValue(x, y);
                node.gCost = int.MaxValue;
                node.CalculateFCost();
                node.cameFrom = null;
            }
        }

        startNode.gCost = 0;
        startNode.hCost = CalculateDistance(startNode, endNode);
        startNode.CalculateFCost();

        while(openList.Count > 0) {
            Node currentNode = GetLowestFCostNode(openList);
            if(currentNode == endNode) {
                return CalculatePath(endNode);
            } else {
                openList.Remove(currentNode);
                closedList.Add(currentNode);

                foreach(Node neighbourNode in GetNeighborList(currentNode)) {
                    if(closedList.Contains(neighbourNode)) continue;

                    int tentativeGCost = currentNode.gCost + CalculateDistance(currentNode, neighbourNode);
                    if(tentativeGCost < neighbourNode.gCost) {
                        neighbourNode.cameFrom = currentNode;
                        neighbourNode.gCost = tentativeGCost;
                        neighbourNode.hCost = CalculateDistance(neighbourNode, endNode);
                        neighbourNode.CalculateFCost();

                        if(!openList.Contains(neighbourNode)) {
                            openList.Add(neighbourNode);
                        }
                    }
                }
            }
        }
        return null;
    }


    List<Node> GetNeighborList(Node currentNode) {
        List<Node> neighbourList = new List<Node>();
        if(currentNode.x-1 >= 0) {
            neighbourList.Add(grid.GetValue(currentNode.x - 1, currentNode.y));

            if(currentNode.y - 1 >= 0) neighbourList.Add(grid.GetValue(currentNode.x - 1, currentNode.y + 1));

            if(currentNode.y + 1 < grid.Height) neighbourList.Add(grid.GetValue(currentNode.x - 1, currentNode.y + 1));
        }
        if(currentNode.x + 1 < grid.Width) {
            neighbourList.Add(grid.GetValue(currentNode.x + 1, currentNode.y));

            if(currentNode.y - 1 >= 0) neighbourList.Add(grid.GetValue(currentNode.x + 1, currentNode.y - 1));

            if(currentNode.y + 1 >= 0) neighbourList.Add(grid.GetValue(currentNode.x + 1, currentNode.y - 1));
        }
        if(currentNode.y - 1 >= 0) neighbourList.Add(grid.GetValue(currentNode.x, currentNode.y - 1));

        if(currentNode.y + 1 < grid.Height) neighbourList.Add(grid.GetValue(currentNode.x, currentNode.y + 1));

        return neighbourList;
    }

    List<Node> CalculatePath(Node endNode) {
        List<Node> path = new List<Node>();
        path.Add(endNode);
        Node currentNode = endNode;
        while(currentNode.cameFrom != null) {
            path.Add(currentNode.cameFrom);
            currentNode = currentNode.cameFrom;
        }
        path.Reverse();
        return path;
    }



    int CalculateDistance(Node a, Node b) {
        int xDistance = Mathf.Abs(a.x - b.x);
        int yDistance = Mathf.Abs(a.y - b.y);
        int remaining = Mathf.Abs(xDistance - yDistance);
        return MOVE_DIAGONAL_COST * Mathf.Min(xDistance, yDistance) + MOVE_STRAIGHT_COST * remaining;
    }

    Node GetLowestFCostNode(List<Node> pathNodeList) {
        Node lowestFCostNode = pathNodeList[0];
        for(int i = 1; i < pathNodeList.Count; i++) {
            if(pathNodeList[i].fCost < lowestFCostNode.fCost) {
                lowestFCostNode = pathNodeList[i];
            }
        }
        return lowestFCostNode;
    }
}
