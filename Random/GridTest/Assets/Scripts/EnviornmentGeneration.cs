using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnviornmentGeneration : MonoBehaviour{
    [SerializeField] Camera cam;
    Grid<int> platform = new Grid<int>(20, 10, 5f, new Vector3());

    private void Start() {
        platform.DrawGrid();
        platform.DrawTextOnGrid();
    }

    private void Update() {
        if(Input.GetMouseButtonDown(0)) {
            platform.SetValue(GetMousePosition(),platform.GetValue(GetMousePosition())+1);
            //Debug.Log(platform.GetValue(GetMousePosition()));
        }
    }

    private Vector3 GetMousePosition() {
        Vector3 vec = cam.ScreenToWorldPoint(Input.mousePosition);
        vec.z = 0f;
        return vec;
    }
}
