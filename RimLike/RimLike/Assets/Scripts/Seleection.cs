using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Seleection : MonoBehaviour {
    [SerializeField] Camera camera;
    [SerializeField] GameManager GameManagerObj;

    private void Update() {
        if(Input.GetMouseButtonDown(0)) {
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if(Physics.Raycast(ray, out hit)) {
                switch(hit.transform.tag) {
                    case "Player":
                        hit.transform.GetComponent<Player>().GetSelected();
                        break;
                }
            }
        }

        if(Input.GetMouseButtonDown(1)) {
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if(Physics.Raycast(ray, out hit)) {
                switch(GameManagerObj.selected.tag) {
                    case "UnTagged":
                        break;
                    case "Player":
                        GameManagerObj.selected.GetComponent<Player>().Move(hit.transform.position);
                        break;
                }
            }
        }
    }
}
