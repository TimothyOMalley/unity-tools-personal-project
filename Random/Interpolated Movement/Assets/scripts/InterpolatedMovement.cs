using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InterpolatedMovement : MonoBehaviour{
    float t;
    private void Start() {
        float t = Time.time;
    }
    
    private void Update() {
        if(t > 4) { t = 4; } else { t += Time.deltaTime; }
        float newX = (float)(0.125)*(3-2 * t)*t*t;
        float newY = (float)Mathf.Sin(t) * 4;
        Vector3 newPos = new Vector3(newX, newY, 0);
        transform.position = newPos;
    }
}
